import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.ArrayList;
import java.text.DecimalFormat;

public class Task16 {

    public static void printInstructions() {
        System.out.println("Please provide arguments in following format: 'java Task16.java <search string> <filename>(optional)'");
        System.out.println("Example: 'java Task16.java myword small.txt'");
    }


    public static void main(String[] args) throws IOException {
        // CHANGE TO INPUT BY USER
        String fileName = "small.txt";
        String searchString = "";

        if (args.length == 0){
            printInstructions();
            System.exit(0) ;
        }
        if (args.length == 1){
            searchString = args[0];
        }
        if (args.length == 2){
            searchString = args[0];
            fileName = args[1];
        }

        searchString = searchString.toLowerCase();
        // Set decimalformat to only get 2 decimals
        DecimalFormat doubleFormat = new DecimalFormat("#.00");


        ArrayList<String> fileLines = new ArrayList<String>();

        //using Scanner class for large files, to read line by line
        Path path = Paths.get(fileName);
        Scanner scanner = new Scanner(path);
        File file = new File(fileName);
        if (!file.exists() || !file.isFile()) return;

        double fileSizeInKb = (double)file.length() / 1024.0;
        //System.out.println("Read text file using Scanner");
        //read line by line
        while(scanner.hasNextLine()){
            //process each line
            fileLines.add(scanner.nextLine());
            //System.out.println(line);
        }
        System.out.println("Name of file: " + file.getName() +" (Size on disk: " + doubleFormat.format(fileSizeInKb) + "kB)");
        System.out.println("Number of lines in file: " + fileLines.size());

        int nOccurances =0, nWords =0;
        for (String line : fileLines) {
            String[] lineSplitInWords = line.split(" ");    

            for ( String s : lineSplitInWords) {
                nWords++;
                if (s.toLowerCase().contains(searchString)) nOccurances++;
            }
        }
        System.out.println("Number of words in file: " + nWords);
        System.out.println("Search term \"" + searchString + "\" found " + nOccurances + " times.");

        scanner.close();

    }

    
}